(define-library
  (retropikzel json v0.2.0 main)
  (import (scheme base)
          (scheme read)
          (scheme write)
          (scheme file)
          (scheme char))
  (export json-parse
          json-parse-to-string
          json-parse-file
          json-stringify)
  (begin

    (define slurp
      (lambda (file-name)
        (letrec ((read-all (lambda (result line)
                             (if (eof-object? line)
                               result
                               (read-all (cons line result)
                                         (read-string 4000))))))
          (with-input-from-file
            file-name
            (lambda ()
              (apply string-append
                     (reverse (read-all (list) (read-string 4000)))))))))

    (define json-char->scheme-char
      (lambda (char object-type result)
        (cond
          ((char=? char #\newline) (cons #\space result))
          ((char=? char #\[) (cons #\( (cons #\# result)))
          ((char=? char #\]) (cons #\space (cons #\) result)))
          ((char=? char #\{) (cons #\( (cons #\( result)))
          ((char=? char #\}) (cons #\) (cons #\) result)))
          ((char=? char #\:) (cons #\space (cons #\. (cons #\space result))))
          ((char=? char #\,) (if (eq? object-type 'object)
                               (cons #\( (cons #\space (cons #\) result)))
                               result))
          (else (cons char result)))))

    (define json-parse-internal
      (lambda (char text object-type result)
        (if (null? text)
          (json-char->scheme-char char object-type result)
          (json-parse-internal
            (car text)
            (cdr text)
            (cond ((char=? char #\{) 'object)
                  ((char=? char #\}) 'none)
                  ((char=? char #\[) 'list)
                  (else object-type))
            (json-char->scheme-char char object-type result)))))

    (define json-parse
      (lambda (text)
        (let* ((text-list (string->list text))
               (result (json-parse-internal (car text-list)
                                            (cdr text-list)
                                            'object
                                            (list))))
          (read (open-input-string (list->string (reverse result))))
          ;(list->string (reverse result))

          )))

    (define json-parse-file
      (lambda (path)
        (json-parse (slurp path))))


    (define json-stringify-internal
      (lambda (object)
        (cond
          ((vector? object)
           (let ((items-count (vector-length object))
                 (index 0))
             (append
               (list "[")
               (apply append
                      (map (lambda (item)
                             (set! index (+ index 1))
                             (append (json-stringify-internal item)
                                     (list (if (< index items-count)
                                             ", "
                                             ""))))
                           (vector->list object)))
               (list "]"))))
          ((and (list? object)
                (> (length object) 0)
                (pair? (car object)))
           ;(display "LIST: ")
           ;(write object)
           ;(display " ")
           ;(display "Is also pair?: ")
           ;(display (pair? object))
           ;(newline)
           (let ((items-count (length object))
                 (index 0))
             (append
               (list "{")
               (apply append
                      (map (lambda (item)
                             (set! index (+ index 1))
                             (append (json-stringify-internal item)
                                     (list (if (< index items-count)
                                             ", "
                                             ""))))
                           object))
               (list "}"))))
          ((pair? object)
           ;(display "PAIR: ")
           ;(write object)
           ;(newline)
           (append
             (json-stringify-internal (car object))
             (list ": ")
             (json-stringify-internal (cdr object))))
          ((string? object)
           (list
             (parameterize
               ((current-output-port
                  (open-output-string)))
               (display #\")
               (display object)
               (display #\")
               (get-output-string (current-output-port)))))
          ((number? object)
           (list
             (parameterize
               ((current-output-port
                  (open-output-string)))
               (display object)
               (get-output-string (current-output-port)))))
          (else
            (list
              (parameterize
                ((current-output-port
                   (open-output-string)))
                (display object)
                (get-output-string (current-output-port))))))))

    (define json-stringify
      (lambda (object)
        (apply string-append (json-stringify-internal object))))))
