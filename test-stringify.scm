(import (scheme base)
        (scheme read)
        (scheme write)
        (scheme file)
        (scheme process-context)
        (retropikzel json v0-1-0 main))

(define slurp
  (lambda (file-name)
    (letrec ((read-all (lambda (result line)
                         (if (eof-object? line)
                           result
                           (read-all (string-append result line)
                                     (read-string 4000))))))
      (with-input-from-file
        file-name
        (lambda ()
          (read-all "" (read-string 4000)))))))

(define path (list-ref (command-line) 1))
(define input-path (string-append "./tmp/" path ".scm"))
(define output-path (string-append "./tmp/" path))
(display "Stringifying file: ")
(display path)
(newline)

(define object (with-input-from-file input-path (lambda () (read))))
(with-output-to-file output-path (lambda () (display (json-stringify object))))
