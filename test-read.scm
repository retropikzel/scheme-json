(import (scheme base)
        (scheme write)
        (scheme read)
        (scheme file)
        (scheme process-context))


(define path (string-append "./tmp/" (list-ref (command-line) 1) ".scm"))
(define data
  (with-input-from-file
    path
    (lambda ()
      (read))))
(exit)
