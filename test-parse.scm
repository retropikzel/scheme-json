(import (scheme base)
        (scheme read)
        (scheme write)
        (scheme file)
        (scheme process-context)
        (retropikzel json v0-1-0 main))

(define path (list-ref (command-line) 1))
(define output-path (string-append "./tmp/" path ".scm"))
(display "Parsing file:")
(display path)
(newline)
(define test-file
  (lambda (path)
    (with-output-to-file
      output-path
      (lambda ()
        (write (json-parse-file path))
        (newline)))))


(test-file path)
