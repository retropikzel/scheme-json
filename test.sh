#!/bin/env bash

set -euo pipefail

FILE=$1

mkdir -p tmp/test-data

cat ${FILE} | jq > /dev/null
time gosh -r7 -A . test-parse.scm ${FILE}
gosh -r7 test-read.scm ${FILE}
time gosh -r7 -A . test-stringify.scm ${FILE}
cat tmp/${FILE} | jq > /dev/null
