#!/usr/env bash

set -euo pipefail

mkdir -p tmp

for file in ./test-data/*
do
    bash test.sh "${file}"
done
